package ass2client;

import javax.servlet.http.HttpServletRequest;

import org.apache.cxf.endpoint.ClientImpl.EchoContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import autotest.*;
import autotestwsdl.*;






@Controller
public class Ass2Controller {
	@Autowired
	   AutotestPortType autotest;
	

   // TODO: Add the TopDownSimpleService interface as a member of the controller
   
   @RequestMapping("/autocheck")
   public String autocheck(@ModelAttribute("request") AutotestReqMsg req, Model m) throws Exception {
      // TODO: return the autocheck results of a person
	  
	  
	  System.out.println(req.getFullname());
	  System.out.println(req.getLicense());
	  System.out.println(req.getPostcode());
	  
	  /*req.setFullname((String) model.get("fullname"));
	  req.setLicense((String) model.get("license"));
	  req.setPostcode((String) model.get("postcode"));*/
	  AutotestRespMsg resp = new AutotestRespMsg();
	  try{
		  resp = autotest.autotest(req);
	  }
	  catch(Exception e){
		  resp.setCrimes("error");
resp.setDetails("error");
e.printStackTrace();
	  }
	  m.addAttribute("checkResponse", resp);
	  m.addAttribute("details", req);
	  
	   
      // TODO: Call the web service      
      
      // TODO: Replace null with the results from the web service response.
      
      
      // View we are returning to, in this case processImportMarketData.jsp 
      return "checkResult";
   }

   @RequestMapping("/searchjobs")
   public String processDownloadFile(ModelMap model) throws Exception {
      // TODO: Add the creation of a DownloadFile request type and populate it

      // TODO: Call the web service      
      
      // TODO: Replace null with the results from the web service response.
      model.addAttribute("returnData", null);
      
      // View we are returning to, in this case processImportMarketData.jsp 
      return "processDownloadFile";
   }
   
   @RequestMapping(value = "/",  method = RequestMethod.GET)
   public String home(Model model){
	   model.addAttribute("request", new AutotestReqMsg());
	   
	   
	   
	   
	   return "home";
   }

}


