<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<style>
<%@ include file="theme1/styling.css" %>
</style>
<title>Assign2 client</title>
</head>
<body>
<div id="topbar">Service Oriented Architectures</div>
<h1>Welcome to Sean and Phillip's web service!</h1>

Autochecker:<br><a></a>
<form:form modelAttribute="request" action="autocheck" method="post">
<b>Import market data function demo</b>
<table>  
    <tbody><tr>
    	<td><form:label path="fullname">Full Name</form:label></td>
    	<td><form:input path="fullname"></form:input></td>
    	</tr><tr>
    	<td><form:label path="license">License</form:label></td>
    	<td><form:input path="license"></form:input></td>
    	</tr><tr>
    	<td><form:label path="postcode">Post Code</form:label></td>
    	<td><form:input path="postcode"></form:input></td>
    	</tr>
     </tbody></table>    
     <input name="" type="submit" value="Save">    
     <input name="" type="reset" value="Reset">
</form:form >


    

</body>
</html>