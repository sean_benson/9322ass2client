<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>AutoCheck Result</title>
<style>
<%@ include file="theme1/styling.css" %>
</style>
</head>
<body>
The following detials were checked:<br>
Name: ${details.fullname}<br>
License: ${details.license}<br>
Postcode: ${details.postcode}<br>
With the following results:<br>
The details ${checkResponse.details} in our database<br>
Their criminal record is ${checkResponse.crimes}<br> 


</body>
</html>